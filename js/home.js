jQuery(document).ready(function($) {
    // Slider Group
    if (Modernizr.mq('only screen and (min-width: 980px)')) {
        var mySwiper = new Swiper ('.main__slider--2 .swiper-container', {
            slidesPerView: 5,
            nextButton: '.main__slider--2 .swiper-button-next',
            prevButton: '.main__slider--2 .swiper-button-prev'
        })

    }  else {
         var mySwiper = new Swiper ('.main__slider--2 .swiper-container');
        
        mySwiper.destroy(true, true);
    }

    // Slider what's new
    var mySwiper2 = null;
    if (Modernizr.mq('only screen and (min-width: 1600px)')) {
        mySwiper2 = new Swiper ('.main__slider--3 .swiper-container', {
            slidesPerView: 4,
            spaceBetween: 30,
            nextButton: '.main__slider--3 .swiper-button-next',
            prevButton: '.main__slider--3 .swiper-button-prev'
        })

    } else if (Modernizr.mq('only screen and (min-width: 1140px)')) {
        mySwiper2 = new Swiper ('.main__slider--3 .swiper-container', {
            slidesPerView: 3,
            spaceBetween: 30,
            nextButton: '.main__slider--3 .swiper-button-next',
            prevButton: '.main__slider--3 .swiper-button-prev'
        })

    } else if (Modernizr.mq('only screen and (min-width: 980px)')) {
        mySwiper2 = new Swiper ('.main__slider--3 .swiper-container', {
            slidesPerView: 2,
            spaceBetween: 30,
            nextButton: '.main__slider--3 .swiper-button-next',
            prevButton: '.main__slider--3 .swiper-button-prev'
        })

    }  else {
        if(mySwiper2)
        mySwiper2.destroy(true, true);
    } 

    // Slider our partner
    var mySwiper3 = null;
    if (Modernizr.mq('only screen and (min-width: 1600px)')) {
        var mySwiper3 = new Swiper ('.main__slider--4 .swiper-container', {
            slidesPerView: 14,
            loop: true,
            nextButton: '.main__slider--4 .swiper-button-next',
            prevButton: '.main__slider--4 .swiper-button-prev'
        })

    }
    else if (Modernizr.mq('only screen and (min-width: 1280px)')) {
        var mySwiper3 = new Swiper ('.main__slider--4 .swiper-container', {
            slidesPerView: 12,
            loop: true,
            nextButton: '.main__slider--4 .swiper-button-next',
            prevButton: '.main__slider--4 .swiper-button-prev'
        })

    } 
    else if (Modernizr.mq('only screen and (min-width: 980px)')) {
        var mySwiper3 = new Swiper ('.main__slider--4 .swiper-container', {
            slidesPerView: 9,
            loop: true,
            nextButton: '.main__slider--4 .swiper-button-next',
            prevButton: '.main__slider--4 .swiper-button-prev'
        })

    }  else {
        if(mySwiper3)
        mySwiper3.destroy(true, true);
    } 

    $(window).on("debouncedresize", function( event ) {
      // Slider Group
        if (Modernizr.mq('only screen and (min-width: 980px)')) {
            var mySwiper = new Swiper ('.main__slider--2 .swiper-container', {
                slidesPerView: 5,
                nextButton: '.main__slider--2 .swiper-button-next',
                prevButton: '.main__slider--2 .swiper-button-prev'
            })

        }  else {
             var mySwiper = new Swiper ('.main__slider--2 .swiper-container');
            
            mySwiper.destroy(true, true);
        }

        // Slider what's new
        var mySwiper2 = null;
        if (Modernizr.mq('only screen and (min-width: 1600px)')) {
            mySwiper2 = new Swiper ('.main__slider--3 .swiper-container', {
                slidesPerView: 4,
                spaceBetween: 30,
                nextButton: '.main__slider--3 .swiper-button-next',
                prevButton: '.main__slider--3 .swiper-button-prev'
            })

        } else if (Modernizr.mq('only screen and (min-width: 1140px)')) {
            mySwiper2 = new Swiper ('.main__slider--3 .swiper-container', {
                slidesPerView: 3,
                spaceBetween: 30,
                nextButton: '.main__slider--3 .swiper-button-next',
                prevButton: '.main__slider--3 .swiper-button-prev'
            })

        } else if (Modernizr.mq('only screen and (min-width: 980px)')) {
            mySwiper2 = new Swiper ('.main__slider--3 .swiper-container', {
                slidesPerView: 2,
                spaceBetween: 30,
                nextButton: '.main__slider--3 .swiper-button-next',
                prevButton: '.main__slider--3 .swiper-button-prev'
            })

        }  else {
            if(mySwiper2)
            mySwiper2.destroy(true, true);
        } 

        // Slider our partner
        var mySwiper3 = null;
        if (Modernizr.mq('only screen and (min-width: 1600px)')) {
            var mySwiper3 = new Swiper ('.main__slider--4 .swiper-container', {
                slidesPerView: 14,
                loop: true,
                nextButton: '.main__slider--4 .swiper-button-next',
                prevButton: '.main__slider--4 .swiper-button-prev'
            })

        }
        else if (Modernizr.mq('only screen and (min-width: 1280px)')) {
            var mySwiper3 = new Swiper ('.main__slider--4 .swiper-container', {
                slidesPerView: 12,
                loop: true,
                nextButton: '.main__slider--4 .swiper-button-next',
                prevButton: '.main__slider--4 .swiper-button-prev'
            })

        } 
        else if (Modernizr.mq('only screen and (min-width: 980px)')) {
            var mySwiper3 = new Swiper ('.main__slider--4 .swiper-container', {
                slidesPerView: 9,
                loop: true,
                nextButton: '.main__slider--4 .swiper-button-next',
                prevButton: '.main__slider--4 .swiper-button-prev'
            })

        }  else {
            if(mySwiper3)
            mySwiper3.destroy(true, true);
        } 
    });
});