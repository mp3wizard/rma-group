jQuery(document).ready(function($) {
    if (Modernizr.mq("screen and (min-width:980px)")) {
        $('.section__column').columnize({ columns: 2 });
    }

    $(window).on("debouncedresize", function( event ) {
        if (Modernizr.mq("screen and (min-width:980px)")) {
            $('.section__column').columnize({ columns: 2 });
        } else {
            $('.section__column').columnize({ columns: 1 });
        }
    });
}); 