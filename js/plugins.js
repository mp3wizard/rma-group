// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.
Modernizr.load([
    {
        test : Modernizr.mq(),
        nope : '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js'
    },
    {
        test : Modernizr.rgba,
        nope : '//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js'
    }
]);
jQuery(document).ready(function($) {
    // Check Link and auto addclass active
    var file_name = document.location.href; 
var end = (file_name.indexOf("?") == -1) ? file_name.length : file_name.indexOf("?"); 
jQuery("a[href='" + file_name.substring(file_name.lastIndexOf("/")+1, end) + "']").addClass("active"); 

    // Side Navigation for mobile & tablet
     $(".button-collapse").sideNav({
        edge: 'right',
        closeOnClick: true
     });

    // Language Switcher
    $("#lang_switch").selectBoxIt();

    // Slider Key Visual
    $(".page__home .main__slider--1 .royalSlider").royalSlider({
        // options go here
        keyboardNavEnabled: true,
        imageScaleMode: 'fill',
        arrowsNav: false,
        loop: true,
        autoPlay: {
            // autoplay options go gere
            enabled: true,
            pauseOnHover: true,
            delay: 5000
        }
    });

    // Slider Key Visual Inside Page
    $(".page__inside .main__slider--1 .royalSlider").royalSlider({
        // options go here
        keyboardNavEnabled: true,
        imageScaleMode: 'fill',
        arrowsNav: true,
        controlNavigation:'none',
        arrowsNavAutoHide: false,
        loop: true,
        slidesSpacing: 0,
        autoPlay: {
            // autoplay options go gere
            enabled: true,
            pauseOnHover: true,
            delay: 5000
        }
    }); 

    
});